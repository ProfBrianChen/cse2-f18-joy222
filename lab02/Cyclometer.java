/*Joshua Yang
September 7th, 2018
This program's purpose is to print the # of minut*/
public class Cyclometer{
  public static void main (String [] args){
    int NumSecTrip1 = 480; // Number of seconds it took for trip #1
    int NumSecTrip2 = 3220; // Number of seconds it took for trip #2
    int NumCountsTrip1 = 1561; // Number of counts it took for trip #1
    int NumCountsTrip2 = 9037; // Numbre of counts it took for trip #2
    double wheeldiameter = 27.0; // The diameter of the wheeldiameter
    double PI = 3.14159; // value of pi
    int feetpermile = 5280; // number of feet in one mile
    int inchesPerFoot = 12; // number of inches in one foot
    double SecsPerMin = 60.0; // number of seconds per minute
    double DistanceTrip1, DistanceTrip2, TotDistance; // distance for each trip and the total distance
    System.out.println("Trip 1 took "+
       	     (NumSecTrip1 / SecsPerMin)+" minutes and had "+
       	      NumCountsTrip1+" counts.");
    //Trip 1 took 8 minutes and had 1561 counts.
	       System.out.println("Trip 2 took "+
       	     (NumSecTrip2 / SecsPerMin)+" minutes and had "+
       	      NumCountsTrip2+" counts.");
    //Trip 2 took 58.888 minutes and had 9037 counts.
    DistanceTrip1 = NumCountsTrip1 * wheeldiameter * PI; // gives Trip#1's distance in inches
    DistanceTrip1 /= inchesPerFoot * feetpermile; // gives trip #1's distance in miles
	  DistanceTrip2 = NumCountsTrip2 * wheeldiameter * PI/inchesPerFoot / feetpermile; // gives trip #2's distance in miles
	  TotDistance = DistanceTrip1 + DistanceTrip2; // gives the value of total distance of trip #1 and #2 added
    System.out.println("Trip 1 was "+DistanceTrip1+" miles"); // prints out the distance of trip #1 in miles
	  System.out.println("Trip 2 was "+DistanceTrip2+" miles"); // prints out the distance of trip #2 in miles
	  System.out.println("The total distance was "+ TotDistance+" miles"); // prints out the total distance of trip #1 and trip #2 in miles



  }
}