import java.util.*;//imports everything 
import java.util.Arrays;
public class TicTacToe {
	public static String[][] newboard() //method that creates a board
	{
		String[][] ticboard = new String[3][3];
		int i = 1;
		for (int x = 0; x < ticboard.length; x++)
		{
			for (int y = 0; y < ticboard[x].length; y++)
			{
				ticboard[x][y] = i + "";
				i++;				
			}
		}
		return ticboard;
	}
	//method that asks for an input, checks it, and returns the board after assigning either an "X" or an "O".
	public static String[][] getUserInput(Scanner sc, String[][] board, String player)
	{
		int pn = 0;
		while (true)
		{
			System.out.println("Enter the position number for player " + player + ": ");
			while (sc.hasNextInt() == true) //checks if it is an integer
			{
				pn = sc.nextInt();
				if (pn > 0 && pn < 10) //checks range
				{
					for (int x = 0; x < board.length; x++)
					{
						for (int y = 0; y < board[x].length; y++)
						{
							if (board[x][y].compareTo(pn + "") == 0)//checks if the selected spot is overlapped.
							{
								board[x][y] = player;//if not, assign
								return board;
							}
						}
					}
				}
				System.out.println("Cannot do that."); //if the spot is taken, repeat the loop
				System.out.println("Enter the position number for player " + player + ": ");
			}
		}
	}
	
	public static void printBoard(String[][] board)//prints the board
	{
		for (int x = 0; x < board.length; x++)
		{
			for (int y = 0; y < board[x].length; y++)
			{
				System.out.print(board[x][y] + " ");	
			}
			System.out.println();
		}
	}
	//will return "X", "Y", or nothing if the game is still going
	public static String getWinner(String[][] board)
	{
		String winner = "";
		for (int i = 0; i < board.length; i++)
		{
			if (board[i][0] == board[i][1] && board[i][1] == board[i][2])
			{
				winner = board[i][0];
				return winner;
			}
		}
		
		for (int j = 0; j < board.length; j++)
		{
			if (board[0][j] == board[1][j] && board[1][j] == board[2][j])
			{
				winner = board[0][j];
				return winner;
			}
		}
		
		if (board[0][0] == board[1][1] && board[1][1] == board[2][2])
		{
			winner = board[0][0];
			return winner;
		}
		
		else if (board [2][0] == board[1][1] && board[1][1] == board[0][2])
		{
			winner = board[2][0];
			return winner;
		}
		
		return "";
	}
	public static void main(String[] args)
	{
		Scanner pos = new Scanner(System.in);
		String currentplayer = "X";
		String[][] board = newboard(); //creates a board
		printBoard(board);
		for (int i = 0; i < 9; i++)
		{
			String winner = getWinner(board);
			if (winner.compareTo("") == 0) //if there is no winner yet, alternate the turns
			{
				getUserInput(pos, board, currentplayer);
				if (currentplayer.compareTo("O") == 0)
				{
					currentplayer = "X";
				}
				
				else
				{
					currentplayer = "O";
				}
			}
			
			else if (winner.compareTo("X") == 0)
			{
				System.out.println("Player \"X\" won!");
				break;
			}
			
			else if (winner.compareTo("O") == 0)
			{
				System.out.println("Player \"O\" won!");
				break;
			}
			
			if (i == 8)
			{
				System.out.println("It is a draw");
				break;
			}
			
			printBoard(board);
		}

	}
}
