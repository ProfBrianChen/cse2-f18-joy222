import java.util.Scanner; //importing Scanner

public class Lab05 {
	public static void main (String []args) {
		int coursenumber = 1; //initiallizing variables
		String addtwozeros = "00";
		String addonezero = "0";
		String coursenum = "";
		String Dptname = "";
		int Numtimesaweek = 0;
		int classhour = 0;
		int classminute = 0;
		String classminutenum = "";
		String amorpm = "";
		String profname = "";
		int numStudent = 0;
		Scanner s = new Scanner(System.in);
		
	//asks the course number
	System.out.println("enter your course number:");
	while (true)
	{	
		if (s.hasNextInt() == true) //if int entered, go to next if
		{
			coursenumber = s.nextInt();
			
			if (coursenumber < 999) //if # is < 999, break out
			{
				break;
			}
		}
		
		System.out.println("Invalid course number");
		System.out.println("enter your course number:"); //error msg
		
		s.nextLine();
	}
	if (coursenumber < 999) {
		coursenum = String.format("%03d" , coursenumber); //adds two 0s if it's like 1, adds one 0 if it's like 10 (ex: 1 -> 001 10-> 010)
	}
	
	//asks the department name
	System.out.println("enter your department name:");
	
	s.nextLine();
	Dptname = s.nextLine();
	
	while (Dptname.length() == 0) //if nothing is entered, error msg
	{			
		System.out.println("Invalid deparment name.");
		System.out.println("enter your department name:");
	
		Dptname = s.nextLine();
	}
	
	//asks the number of times he/she meets in a week
	System.out.println("enter the number of times you meet in a week:");
	
	while (true)
	{
		if (s.hasNextInt() == true) //if int is entered, enter the if statements
		{
			Numtimesaweek = s.nextInt();
			
			if (Numtimesaweek < 8 && Numtimesaweek > -1)
			{
				break;
			}
			
		}
		
		System.out.println("Invalid number of times a week:"); //errors
		System.out.println("enter the number of times you meet in a week:");
		
		s.nextLine();
	}
	
	//asks when the class meets
	System.out.println("enter the hour of the time you meet: (ex: 1 p.m -> 13)");
	
	while (true) 
	{
		if (s.hasNextInt() == true) // if has integer for input, then enter 
		{
			classhour = s.nextInt();
			
			if (classhour > 6 && classhour < 12)
			{
				amorpm = "am"; //am or pm depending on the hour
				break;
			}
			
			if (classhour == 12)
			{
				amorpm = "pm";
				classhour = 12;
				break;
			}
			
			if (classhour > 12 && classhour < 24)
			{
				amorpm = "pm";
				classhour = classhour - 12;
				break;
			}
			
		}
		
		System.out.println("Invalid hour"); //error msg
		System.out.println("enter the hour of the time you meet: (ex: 1 p.m -> 13)");
		
		s.nextLine();
	}
	
	//asks the time (minutes) of the class
	System.out.println("enter the minute of the time you meet: (ex: 1:25 p.m -> 25)");
	while (true)
	{
		if (s.hasNextInt() == true)
		{
			classminute = s.nextInt(); 
			
			if (classminute > -1 && classminute < 60)
			{
				classminutenum = String.format("%02d", classminute); //formats the minute just like course number
				break;
			}
		}
		System.out.println("Invalid class minute.");
		System.out.println("enter the minute of the time you meet: (ex: 1:25 p.m -> 25)");
		
		s.nextLine();
	}
	
	//asks the instructor's name
	System.out.println("enter the instructor's name:");
	
	s.nextLine();
	profname = s.nextLine();

	while ((profname.length() == 0)) //if nothing is entered, error msg
	{
		System.out.println("invalid instructor name.");
		System.out.println("enter the instructor's name:");
		
		profname = s.nextLine();
		
	}
	
	//asks the number of students
	System.out.println("enter the number of students in the class:");
	while (true)
	{
		if (s.hasNextInt() == true) //if integer is entered, numstudents is equal the the integer
		{
			numStudent = s.nextInt();
			break;
		}
		
		System.out.println("Invalid student number");
		System.out.println("enter the number of students in the class:");
	}
	
		System.out.println("Your course number is " + coursenum + "."); //prints out the final results
		System.out.println("Your department name is \"" + Dptname + "\".");
		System.out.println("You meet " + Numtimesaweek + " times a week.");
		System.out.println("You meet at " + classhour + ":" + classminutenum + amorpm + ".");
		System.out.println("Your instructor's name is : " + profname + ".");
		System.out.println("There are " + numStudent + " students in your class.");
	}
	

	
	
	}
	
	
	


