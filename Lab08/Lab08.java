import java.util.Arrays; //imports arrays
public class Lab08 {
	public static void main (String[]args)
	{
		int[] array1 = new int[100]; //creates two arrays with size 100
		int[] array2 = new int[100];
		for (int i = 0; i < array1.length; i++)
		{
			array1[i] = (int)(Math.random()*100); //assigns array1 with 100 random numbers
		}
		//prints out the numbers in the arrays
		System.out.print("Array 1 holds the following integers: " + Arrays.toString(array1).replace("[", "").replace(",", "")); 
		System.out.println("");
		for (int j = 0; j < array1.length; j++) //forloops for counting and assigning it to array 2
		{
			for (int k = 0; k < array1.length; k++)
			{
				if (array1[j] == k)
				{
					array2[k] = array2[k] + 1; //counting
				}
			}
		}
		
		for (int o = 0; o < array1.length; o++) //prints out the final statements
		{
			System.out.println(o + " occurs " + array2[o] + " times.");
		}
	}
}
