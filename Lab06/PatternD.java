import java.util.Scanner; //same as pattern A...

public class PatternD {
	public static void main (String []args)
	{
		int numRow = 0;
		int number = 1;
		Scanner patternD = new Scanner(System.in);
		
		System.out.println("Enter an integer between 1 and 10");
		while (true)
		{
			if (patternD.hasNextInt() == true)
			{
				number = patternD.nextInt();
				
				if(number > 0 && number < 11)
				{
					break;
				}
			}
			System.out.println("That is not an integer!");
			System.out.println("Enter an integer between 1 and 10");
			patternD.nextLine(); //same as pattern A...
		}
		
		for (numRow = 1; numRow <= number; number--) //user input decreases as the outer forloops runs multiple times
		{
			for (int num = number; num >= 1; num--) // prints out number starting with the user input, decreased by 1 each time
			{
				System.out.print(num + " ");
			}
			System.out.println("");
		}
	}
}
