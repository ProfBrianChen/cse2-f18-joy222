import java.util.Scanner; //imports scanner
public class PatternA {
	public static void main (String []args)
	{
		Scanner patternA = new Scanner(System.in);
		int number = 0; //declares variable
		int numRows = 1;
		
		System.out.println("Enter an integer between 1 and 10"); //tells the user to enter a int between 1 and 10.
		while (true)
		{
			if (patternA.hasNextInt() == true) //if the user enters an integer, go into the if statement
			{
				number = patternA.nextInt(); //number is equal to the user input
				
				if(number > 0 && number < 11) //checks if the number is between 1 and 10
				{
					break; //if so, break
				}
			}
			System.out.println("That is not an integer!"); //error msgs
			System.out.println("Enter an integer between 1 and 10");
			patternA.nextLine();
		}
		
		for (numRows = 1; numRows <= number; numRows++) //Row numbers increase
		{
			for(int num = 1; num <= numRows; num++) //numbers printed out increases by 1 everytime
			{
				System.out.print(num + " ");
			}
			
			System.out.println("");
		}
	}
}
