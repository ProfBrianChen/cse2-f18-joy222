import java.util.Scanner; //imports scanner

public class PatternB {
	public static void main (String []args)
	{
		int number = 0;
		int numRows = 1;
		Scanner patternB = new Scanner (System.in);
		
		System.out.println("Enter an integer between 1 and 10"); //same as pattern A....
		while (true)
		{
			if (patternB.hasNextInt() == true)
			{
				number = patternB.nextInt();
				
				if(number > 0 && number < 11)
				{
					break;
				}
			}
			System.out.println("That is not an integer!");
			System.out.println("Enter an integer between 1 and 10"); //same as pattern A...
			patternB.nextLine();
		}
		
		for (numRows = 1; numRows <= number; number--) // the user input decreases as the forloops run
		{
			for (int i = 1; i <= number; i++) // until the number being printed out equals the user input, it keeps printing
				{
					
					System.out.print(i + " ");
					
				}
				System.out.println("");
			
			
		}
	}
}
