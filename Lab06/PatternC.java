import java.util.Scanner; //same as pattern A...

public class PatternC {
	public static void main (String []args)
	{
		int numRow = 0;
		int number = 1;
		Scanner patternC = new Scanner(System.in);
		
		System.out.println("Enter an integer between 1 and 10");
		while (true)
		{
			if (patternC.hasNextInt() == true)
			{
				number = patternC.nextInt();
				
				if(number > 0 && number < 11)
				{
					break;
				}
			}
			System.out.println("That is not an integer!");
			System.out.println("Enter an integer between 1 and 10"); //same as pattern A...
			patternC.nextLine();
		}
		
		for (numRow = 1; numRow <= number; numRow++) //number of rows determined by the user input
		{
			for(int num =1; num <= number - numRow; num++) // adds space everytime as long as the number being printed is less than the user input - number of rows
			{
				System.out.print(" ");
			}
		
			for (int num = numRow; num >= 1; num--) // prints out numbers as long as it is greater than or equal to 1
			{
				System.out.print(num);
			}
			System.out.println("");
		}
	}
}
