//Joshua Yang
/* this program asks the user to input a number from 0 to 100 and prints out n by n size asterisks
 * with blank spaces that results an "x" in the middle.
 */
import java.util.Scanner; //imports scanner

public class EncryptedX {
	public static void main (String []args)
	{
		Scanner input = new Scanner(System.in);
		int number = 0;
		int firstletter = 0;
		int lastletter = 0;
		
		System.out.println("Enter the number for the size of Encrypted X."); //asks the user for the input
		
		while (true)
		{
			if (input.hasNextInt() == true) //while true loop to check if the user entered an integer between 0 and 100
			{
				number = input.nextInt();
				lastletter = number;
				if (number > -1 && number < 101)
				{
					break;
				}
			}
			
			System.out.println("That is not a valid number."); //infinite loop for non-integer input
			System.out.println("Enter the number for the size of Encrypted X.");
			input.nextLine();
		}
			for (int i = 0; i <= number; i ++) //for loop for rows
			{
				for (int j = 0; j <= number; j++)//forloops for printing out stars and spaces
				{
					/*prints out blank spaces for first and last place for each rows, incrementing
					 * firstletter by one and decrementing the lastletter by one
					 */
					if (j == firstletter || j == lastletter) 
					{
						System.out.print(" ");
					}
					else if (firstletter == lastletter)
					{
						System.out.print("*");
					}
					else 
					{
						System.out.print("*");
					}
				}
				firstletter ++;
				lastletter --;
				System.out.println(""); //for putting them into separate rows after printing out stars and spaces

			}	
		}
		
	}

