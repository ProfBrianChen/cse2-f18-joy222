import java.util.Scanner;

public class Pyramid {
  public static void main (String[]args){
    double squarelength;
    double height;
    double pyramidvolume;
    Scanner dimension = new Scanner(System.in);
    
    System.out.println("The square side of the pyramid is (input length): ");
      squarelength = dimension.nextDouble();
    System.out.println("The height of the pyramid is (input height): ");
      height = dimension.nextDouble(); 
      pyramidvolume = (Math.pow(squarelength, 2.0) * height) / 3.0;
        
    System.out.println("The volume inside the pyramid is: " + pyramidvolume);
    
  }
}