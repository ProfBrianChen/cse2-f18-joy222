//Joshua Yang
//This program converts the quantity of raindrop into cubic miles.
import java.util.Scanner;

public class Convert{
  public static void main (String []args){
    double AffectedAcres; //assgining variables
    double numinches;
    double togallon;
    double cubicmiles;
    Scanner hurricane = new Scanner(System.in);
    
    System.out.println("Enter the affected area in acres: "); //asks for acres
      AffectedAcres = hurricane.nextDouble();
    
    System.out.println("Enter the inches of the rainfall in the affected area: "); //asks the inches for the rainfall
      numinches = hurricane.nextDouble();
    
        togallon = (AffectedAcres * numinches) * (double)27154.2857; //calculate from ares*inches to gallon.

        cubicmiles = togallon / (double)1101117147352.0; //gallon to cubicmiles

    System.out.println("Affected area in cubic miles is: " + cubicmiles + " cubic miles"); //outputs final calculation
    
    
    
      
    
  }
  
}
