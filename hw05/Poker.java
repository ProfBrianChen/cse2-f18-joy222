//Joshua Yang

/* this lab puts 5 random cards in to an array, see if it has onePair, twoPair...,
 *  counts the number of each of them, divides it by the number of hands, 
 *  and finally it gives out the percentage of each occasion.
 */
import java.util.Scanner;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;
public class Poker {
	public static void main (String []args) 
	{
		Scanner hand = new Scanner(System.in);
		int numhand = 0;
		int onePair = 0 , twoPair = 0, threePair = 0, fourPair = 0;
		double oneP = 0.0, twoP = 0.0, threeP = 0.0, fourP = 0.0;
		
		System.out.println("Enter the number of hands you want to generate:"); //infinite loop if the user does not enter an integer
		
		while (true)
		{
			if (hand.hasNextInt() == true)
			{
				numhand = hand.nextInt();
				break;
			}
			System.out.println("That is not an integer!");
			System.out.println("Enter the number of hands you want to generate:");
			hand.nextLine();
		}
		
		for(int iteration = 0; iteration < numhand; iteration++)
		{
			ArrayList<Integer> handlist = new ArrayList<Integer>(4); //creates 5 number arraylist
			int[] pairtracker = new int[13]; //creates a 13 number array that is going to be used to check pairs
			
			for (int j = 0; j <= 4; j++)
			{
				while (true)
				{
					int number = (int)(Math.random()*51)+1;//random number from 1 to 52
					// check to make sure the number isn't already in the list
					if (handlist.contains(number))
					{
						continue;
					}
					
					handlist.add(number);
					break;
				}
			}
	
			// match up the cards
			for(int card : handlist)
			{
				for(int i = 13; i > 0; i--)
				{
					if(card % i == 0)
					{
						pairtracker[i-1]++;
						break;
					}
				}
			}
		
		
			boolean foundonepair = false;
			
			// score the hand based on the matches that we found
			for(int i = 0; i < 13; i++)
			{
				if (pairtracker[i] == 2) // if the number at position i is 2, it's onepair
				{
					// this will only be true on the second pair we find
					if(foundonepair == true)
					{
						twoPair++;
						
						// undo adding to onePair
						onePair--;
						
						continue;
					}
					
					foundonepair = true;
					onePair++;
				}
				
				if (pairtracker[i] == 3) //if score = 3, three of a kind...
				{
					threePair ++;
					
				}
				
				if (pairtracker[i] == 4) //same idea..
				{
					fourPair++;
					
					
				}
				// one set of two cards - one pair
				// two sets of two cards - two pair
				// one set of three cards - three of a kind
				// one set of four cards - four of a kind
	
			}
		}
	

	
		oneP = (double)onePair / (double)numhand; //% for onepair
		twoP = (double)twoPair / (double)numhand; //% for twopair
		threeP = (double)threePair / (double)numhand; //similar ideas...
		fourP = (double)fourPair / (double)numhand;
		//prints out result w/ 3 decimal places
		System.out.println("Number of loops: " + numhand);
		System.out.printf("The probability of Four-of-a-kind: %.3f\n", fourP);
		System.out.printf("The probability of Three-of-a-kind: %.3f\n", threeP);
		System.out.printf("the probability of Two-Pair: %.3f\n", twoP);
		System.out.printf("The probability of One-Pair: %.3f\n", oneP);
		
	}
}
