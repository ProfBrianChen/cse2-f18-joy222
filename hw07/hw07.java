import java.util.Scanner; //imports Scanner

public class hw07 {
	public static String sampleText()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a sample text: "); //asks for sample text
		String text = sc.nextLine();
		
		return text;
	}
	public static void printMenu()
	{
		System.out.println("MENU"); //menu method
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit\n");
		System.out.println("Choose an option:");
	}
	
	public static int getNumOfNonWSCharacters(String maintext)
	{
		int count = 0;
		for (int i = 0; i < maintext.length(); i++)
		{
			if (maintext.charAt(i) != ' ') //count goes up by 1 if the character is not a space
			{
				count++;
			}
		}
		return count;
	}
	
	public static int getNumOfWords(String maintext)
	{
		int counter = 0;
		for (int i = 0; i < maintext.length(); i++) //counter goes up by 1 if there is space between words.
		{
			if (maintext.charAt(i) == ' ')
			{
				counter++;
			}
		}
		counter ++;
		return counter;
	}
	
	public static int findText(String findThis, String maintext) //finds the specific substring from the whole text
	{	
		int i = 0;
		int count = 0;
		
		while (i <= maintext.length() - findThis.length())
		{
			if (maintext.substring(i, i + findThis.length()).equals(findThis))
			{
				count++;
				i = i + findThis.length();
				continue;
			}
			
			i++;
		}
		
		return count; //returns how many times the words appear
	}
	
	public static String replaceExclamation(String maintext)
	{
		String noex = maintext.replace("!", "."); //replaces ! with a period
		return noex;
	}
	
	public static String shortenSpace(String maintext) //replaces two or more spaces with one space
	{
		String modifiedtext = "";
		modifiedtext = maintext.trim().replaceAll("( +)", " ");
		return modifiedtext;
	}
	public static void main (String[]args)
	{
		Scanner sc = new Scanner(System.in);
		String text = sampleText();
		System.out.println("You entered: " + text); //returns the entered text
		printMenu();
		while (true) //while the input has something, reads in the value and determine the option from the menu
		{
			if (sc.hasNext()==true)
			{
				
				String input = sc.next();
				if (input.contains("c"))
				{
					System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text));
					break; 
				}
				
				else if(input.contains("w"))
				{
					System.out.println("Number of words: " + getNumOfWords(text));
					break;
				}
				else if (input.contains("f"))
				{
					System.out.println("Enter a word or phrase to be found: "); //asks for the word to find
					Scanner find = new Scanner(System.in);
					String findthis = find.nextLine();
					System.out.println("\"" + findthis + "\"" + " instances: " + findText(findthis, text));
					break;
				}
				else if (input.contains("r"))
				{
					System.out.println("Edited text: " + replaceExclamation(text)); //returns edited text
				}
				else if (input.contains("s"))
				{
					System.out.println("Edited text: " + shortenSpace(text)); //returns edited text
				}
				else if (input.contains("q"))
				{
					break;
				}
			printMenu();
			}
		}

	}
}