import java.util.Arrays;
import java.util.Scanner;//imports arrays and scanner

public class Lab09 {
	 //int array that makes a new array and copies the elements into it
	public static int[] copy(int[] array1)
	{
		int[] newarray = new int[array1.length];
		for (int i = 0; i < newarray.length; i++)
		{
			newarray[i] = array1[i];
		}
		
		return newarray;
	}
	//inverts the elements in the array
	public static void inverter(int[] inverted)
	{
		for (int i = 0; i < inverted.length / 2; i++)
		{
			int tempval = inverted[i];
			inverted[i] = inverted[inverted.length-1-i];
			inverted[inverted.length-1-i] = tempval;
		}
	}
	//makes a new array using the copy method and inverts it 
	public static int[] inverter2(int[] copyandreturn)
	{
		int[] array = copy(copyandreturn);
		inverter(array);
		
		return array;
		
	}
	//prints out the array using a forloop
	public static void print(int[] printarr)
	{
		for (int i = 0; i < printarr.length; i++)
		{
			System.out.print(printarr[i] + " ");
		}
		System.out.println("");
	}
	public static void main (String[]args)
	{
		// makes a new array, assign elements to the array
		final int arraysize = 10;
		int[] array0 = new int[arraysize];
		for (int i = 0; i <arraysize; i++)
		{
			array0[i] = i;
		}
		//copies the arrays
		int[] array1 = copy(array0);
		int[] array2 = copy(array0);
		//lab instructions
		inverter(array0);
		print(array0);
		//array 1
		inverter2(array1);
		print(array1);
		//array3 using array 2
		int[] array3 = inverter2(array2);
		print(array3);
		 
	}

}
