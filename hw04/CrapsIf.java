/*Joshua Yang
This program asks the user to choose between two random numbers from the dice or input two numbers from 1-6, adds up the number, and prints out
the corresponding result depending on the number using only If statements*/
import java.util.Scanner;
import java.util.Random;
import java.lang.Math; //import Scanner, random, and Math

public class CrapsIf {
  public static void main (String [] args){
    
    int dice1;
    int dice2;
    int typedice1;
    int typedice2;
    int choose; //initializes variable
    
    
    Scanner randortype = new Scanner (System.in);
    Scanner typeyourdice1 = new Scanner (System.in);
    Scanner typeyourdice2 = new Scanner (System.in);//initializes Scanners
    System.out.println("Type \"1\" if you would like to cast random and \"2\" if you would like to cast on your own"); //asks the user for random or input
      choose = randortype.nextInt();
    
    if (choose == 1){ //if the user inputs 1, execute the following commands
      String result = "";
      int rngdice1 = (int)(Math.random()*6)+1;
      int rngdice2 = (int)(Math.random()*6)+1; //random numbers from 1-6
      if ((rngdice1 + rngdice2) == 2) {
        result = "Snake Eyes";
      }
      if ((rngdice1 + rngdice2) == 3) {
        result = "Ace Deuce";
      }
      if ((rngdice1==2) && (rngdice2 ==2 )) {
        result = "Hard four";
      }
      if ((rngdice1 == 3) && (rngdice2 == 1 ) || ((rngdice1 == 1) && (rngdice2 == 3))) {
        result = "Easy Four";
      }
      if ((rngdice1 + rngdice2) == 5) {
        result = "Fever five";
      }
      if ((rngdice1 == 3) && (rngdice2 == 3)) {
        result = "Hard six";
      }
      if (((rngdice1 == 4) && (rngdice2 == 2)) || ((rngdice1 == 1) && (rngdice2 == 5))) {
        result = "Easy six";
      }
      if (((rngdice1 == 3) && (rngdice2 == 5)) || ((rngdice1 == 2) && (rngdice2 == 6))) {
        result = "Easy Eight";
      }
      if ((rngdice1 == 4) && (rngdice2 == 4)) {
        result = "Hard Eight";
      }
      if ((rngdice1 + rngdice2) == 9) {
        result = "Nine";
      }
      if (((rngdice1 == 4) && (rngdice2 == 6)) || ((rngdice1 == 6) && (rngdice2 == 4))) {
        result = "Easy Ten";
      }
      if ((rngdice1 == 5) && (rngdice2 == 5)) {
        result = "Hard Ten";
      }
      if ((rngdice1 + rngdice2) == 11) {
        result = "Yo-leven";
      }
      if ((rngdice1 + rngdice2) == 12) {
        result = "Boxcars";
      }
    System.out.println("You rolled " + rngdice1 + " and " + rngdice2 + ", so it is a(n) " + result + "."); //outputs the result after adding the two values
      
    }
    else {
      System.out.println("Type in the value for dice #1: ");
        typedice1 = typeyourdice1.nextInt();
      System.out.println("Type in the value for dice #2: ");//asks the user for the input
        typedice2 = typeyourdice2.nextInt();
      String result = "";
      if (((typedice1 > 0) && (typedice1 < 7)) && ((typedice2 > 0) && (typedice2 < 7))) {
       
       if ((typedice1 + typedice2) == 2) {
        result = "Snake Eyes";
      }
      if ((typedice1 + typedice2) == 3) {
        result = "Ace Deuce";
      }
      if ((typedice1==2) && (typedice2 ==2 )) {
        result = "Hard four";
      }
      if (((typedice1 == 3) && (typedice2 == 1 )) || ((typedice1 == 1) && (typedice2 == 3))) {
        result = "Easy Four";
      }
      if ((typedice1 + typedice2) == 5) {
        result = "Fever five";
      }
      if ((typedice1 == 3) && (typedice2 == 3)) {
        result = "Hard six";
      }
      if (((typedice1 == 4) && (typedice2 == 2)) || ((typedice1 == 1) && (typedice2 == 5))) {
        result = "Easy six";
      }
      if (((typedice1 == 3) && (typedice2 == 5)) || ((typedice1 == 2) && (typedice2 == 6))) {
        result = "Easy Eight";
      }
      if ((typedice1 == 4) && (typedice2 == 4)) {
        result = "Hard Eight";
      }
      if ((typedice1 + typedice2) == 9) {
        result = "Nine";
      }
      if (((typedice1 == 4) && (typedice2 == 6)) || ((typedice1 == 6) && (typedice2 == 4))) {
        result = "Easy Ten";
      }
      if ((typedice1 == 5) && (typedice2 == 5)) {
        result = "Hard Ten";
      }
      if ((typedice1 + typedice2) == 11) {
        result = "Yo-leven";
      }
      if ((typedice1 + typedice2) == 12) {
        result = "Boxcars";
      }
         System.out.println("You rolled " + typedice1 + " and " + typedice2 + ", so it is a(n) " + result + "."); //prints out the result

      }
  else {
    System.out.println("That is not possible!"); //if neither entered, prints out that is not possible
  }
    }
  
    }
  }

