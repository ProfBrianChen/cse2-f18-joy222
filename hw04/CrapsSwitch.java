/*Joshua Yang
This program asks the user to choose between two random numbers from the dice or input two numbers from 1-6, adds up the number, and prints out
the corresponding result depending on the number using only switch statements*/
import java.util.Scanner;
import java.util.Random;
import java.lang.Math; //imports Scanner, Random, and Math lang

public class CrapsSwitch {
  public static void main (String [] args){
    
    
    int dice1;
    int dice2;
    int typedice1;
    int typedice2;
    int choose; //intializing the variable
    
    
    Scanner randortype = new Scanner (System.in);
    Scanner typeyourdice1 = new Scanner (System.in);
    Scanner typeyourdice2 = new Scanner (System.in);//initializes scanner
    System.out.println("Type \"1\" if you would like to cast random and \"2\" if you would like to cast on your own");//asks the user if random or input
      choose = randortype.nextInt();
    String result = "";
      int rngdice1 = (int)(Math.random()*6)+1;
      int rngdice2 = (int)(Math.random()*6)+1; //random # from range 1-6
    int tworandomadded = rngdice1 + rngdice2;
    
    switch (choose) {
      case 1: dice1 = rngdice1;
              dice2 = rngdice2;
        switch (tworandomadded) { //executes two random numbers, adds them, gets the name of the result
          case 2: result = "Snake Eyes";
            break;
                      
          case 3: result = "Ace Deuce";
            break;
                      
          case 4: 
            switch (dice1) {
              case 1: result = "Easy Four";
              case 3: result = "Easy Four";
                break;
              case 2: result = "Hard Four";
                break;
              
              default: result = "";
                break;
            }
            break;
          case 5: result = "Fever Five";
            break;
            
          case 6: 
            switch (dice1) {
              case 1: result = "Easy Six";
              case 2: result = "Easy Six";
              case 4: result = "Easy Six";
              case 5: result = "Easy Six";
                break;
              case 3: result = "Hard Six";
                break;
              default: result = "";
                break;
                
            }
            break;
            
          case 7: result = "Seven out";
            break;
            
          case 8: 
            switch (dice1) {
              case 2: result = "Easy Eight";
              case 3: result = "Easy Eight";
              case 5: result = "Easy Eight";
              case 6: result = "Easy Eight";
                break;
              case 4: result = "Hard Eight";
                break;
              default: result = "";
                break;
            }
            break;
          case 9: result = "Nine";
            break;
          
          case 10: 
            switch (dice1) {
              case 4: result = "Easy Ten";
              case 6: result = "Easy Ten";
                break;
              case 5: result = "Hard Ten";
                break;
              default: result = "";
                break;
                
            }
            break;
            
          case 11: result = "Yo-leven";
            break;
            
          case 12: result = "Boxcars";
            break;
            default: result = "";
            break;
      
        }
     System.out.println("You rolled " + dice1 + " and " + dice2 + ", " + "and it is a(n) " + result); //prints out result
            break;
      
        
      case 2: System.out.println("Type in the value for dice #1"); //asks for user input for both of the dice values
              typedice1 = typeyourdice1.nextInt();
              System.out.println("Type in the value for dice #2");       
              typedice2 = typeyourdice2.nextInt();
              dice1 = typedice1;
              dice2 = typedice2;
          int twoinputadded = typedice1 + typedice2;

          switch (twoinputadded) {
          case 2: result = "Snake Eyes";
            break;
                      
          case 3: result = "Ace Deuce";
            break;
                      
          case 4: 
            switch (dice1) {
              case 1: result = "Easy Four";
              case 3: result = "Easy Four";
                break;
              case 2: result = "Hard Four";
                break;
              default: result = "";
                break;
            }
            break;
          case 5: result = "Fever Five";
            break;
            
          case 6: 
            switch (dice1) {
              case 1: result = "Easy Six";
              case 2: result = "Easy Six";
              case 4: result = "Easy Six";
              case 5: result = "Easy Six";
                break;
              case 3: result = "Hard Six";
                break;
              default: result = "";
                break;
                
            }
              break;
              
          case 7: result = "Seven out";
            break;
            
          case 8: 
            switch (dice1) {
              case 2: result = "Easy Eight";
              case 3: result = "Easy Eight";
              case 5: result = "Easy Eight";
              case 6: result = "Easy Eight";
                break;
              case 4: result = "Hard Eight";
                break;
              default: result = "";
                break;
            }
              break;
              
          case 9: result = "Nine";
            break;
          
          case 10: 
            switch (dice1) {
              case 4: result = "Easy Ten";
              case 6: result = "Easy Ten";
                break;
              case 5: result = "Hard Ten";
                break;
              default: result = "";
                break;
                
            }
            break;
          case 11: result = "Yo-leven";
            break;
            
          case 12: result = "Boxcars";
            break;
            default: result = "impossible number!";
              break;
            
              
        }
    System.out.println("You rolled " + dice1 + " and " + dice2 + ", " + "and it is a(n) " + result); //prints out result
        break;
        
      default: result = "That's not possible!"; //if neither 1 nor 2 is entered, prints the following statement
        System.out.println(result);
        break;
        
        
      
    }
  }
    }