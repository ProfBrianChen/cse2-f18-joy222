//Joshua Yang
/* This lab prints out 3 random sentences organized by combinations of 10 random subject, 
 * object, noun and verb. Uncommenting String answer part will ask the user
 * if they want another sentence or not 
 */

import java.util.Random; //importing Random, Scanner, Arrays
import java.util.Scanner;
import java.util.Arrays;
public class Methods {
	
	public static int getRandom(int randNum)
	{
		Random Generator = new Random(); //RNG 
		int randomInt = Generator.nextInt(randNum);
		return randomInt;
	}
	
	public static String getAdjective()
	{
		String[] adjs = {"righteous", "flagrant", "immense", "wide-eyed", "eager", "present", "omniscient",
				"fanatical", "murky", "thankful", "hapless", "ruddy"};//list of adjectives
		int index = getRandom(adjs.length);
		return adjs[index]; //picks a random adj
	}
	
	public static String getSubjectNouns()
	{
		String[] subjectNoun = {"ocelot", "porcupine", "silver fox", "dingo", "thorny devil",
				"reindeer", "crocodile", "springbok", "chinchilla", "sloth"};
		int index = getRandom(subjectNoun.length); //lists of subjects and picks a random subject
		return subjectNoun[index];
	}
	
	public static String getPastTenseVerbs()
	{
		String[] verbs = {"yelled", "recorded", "overflowed", "challenged", "slipped", "deceived", "listened",
				"influenced", "remebered", "educated"};
		int index = getRandom(verbs.length);
		return verbs[index]; //lists of past tense verbs and picks a random verb
	}
	
	public static String getObjectNouns()
	{
		String[] verbs2 = {"steer", "burro", "musk-ox", "ermine", "coati", "duckbill platypus", "addax",
				 "monkey", "highland cow", "chamois"};
		int index = getRandom(verbs2.length);
		return verbs2[index]; //lists of random objects and picks a random noun
	}

	public static String thesisSentences()
	{
		String adjective = getAdjective(); //assigns the picked adj, verb, sub,obj in case we need it for later
		String verb = getPastTenseVerbs();
		String subjectNoun = getSubjectNouns();
		String objectNoun = getObjectNouns();
		Scanner yesorno = new Scanner(System.in);
		String thesis = "The " + getAdjective() + " " + getSubjectNouns() + " " + getPastTenseVerbs() + " "  + "the " +
				getObjectNouns() + "."; //assigns thesis in case we want to print it out
		return subjectNoun; //returns the subject for paragraph method
	}
	
	public static String paragraph()
	{
		String subject = thesisSentences();//assigns string subject to randomly picked subject noun
		String sentence1 = "This " + subject + " was " + getAdjective() + " to " + getPastTenseVerbs() + " " + getObjectNouns()+ ".\n";
		int itorname = (int) Math.floor(Math.random()*2); //builds out sentence 1
		if (itorname == 1) //randomly picks between the subject noun and "it"
		{
			subject = "It";
		}
		else
		{
			subject = subject;
		}
		String sentence2 = subject + " " + getPastTenseVerbs() + " " + getObjectNouns() + " to " + getObjectNouns() + ".\n";
		return sentence1 +sentence2; //returns sentence 1 and 2
		
	}
	
	public static String conclusions() //conclusion method
	{
		String subject = thesisSentences();
		
		String conclusion = "This " + subject + " the " + getPastTenseVerbs() + " her " + getObjectNouns() + ".\n";
		return conclusion; //returns conclusion sentence built above
	}
	public static void main (String[]args)
	{
		
		String adjective = getAdjective(); //same thing, in case we need it for later
		String verb = getPastTenseVerbs();
		String subjectNoun = getSubjectNouns();
		String objectNoun = getObjectNouns();
		Scanner yesorno = new Scanner(System.in);
		/*String answer = yesorno.nextLine(); // loop that asks the user for another sentence
		System.out.println("Would you like a sentence? (y/n)");

		while (true)
		{
			if (answer.equals("y"))
			{
			System.out.println("The " + getAdjective() + " " + getAdjective() + " " + getSubjectNouns() +
		" " + getPastTenseVerbs() + " " + "the " + getAdjective() + " " + getObjectNouns() + ".");
		System.out.println("Would you an another random sentence? (y/n)");
		answer = yesorno.nextLine();
			}
			else
			{
				break;
			}
		}*/
		System.out.println(paragraph() + conclusions()); //prints out the paragraph with conclusion sentence
		
	}
}
