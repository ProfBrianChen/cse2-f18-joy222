/*Joshua Yang
This program lets the user input the cost of the bill and divdes 
the cost evenly depending on the number of people.*/
import java.util.Scanner;
public class Check{
    			// main method required for every Java program
   	public static void main(String[] args) {
          Scanner CheckCalc = new Scanner( System.in );
      
          System.out.print("Enter the original cost of the check in the form xx.xx: ");//lets the user enter the original bill
      
            double checkCost = CheckCalc.nextDouble();
      
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );//asks for the % tip
      
            double tipPercent = CheckCalc.nextDouble();
      
               tipPercent /= 100; //converts the percentage into a decimal value
            
          System.out.print("Enter the number of people who went out to dinner: ");
            int numPeople = CheckCalc.nextInt();
      
          double totalCost; //assigns variables
          double costPerPerson;
             int dollars, dimes, pennies;  //whole dollar amount of cost and for storing digits
               
          totalCost = checkCost * (1 + tipPercent); //calculates total cost
          costPerPerson = totalCost / numPeople;    //get the whole amount without decial points
          dollars=(int)costPerPerson;               //only amount of dollars without decial points
          dimes=(int)(costPerPerson * 10) % 10; //number of dimes
          pennies=(int)(costPerPerson * 100) % 10; //number of pennies
      
          System.out.println("Each person in the group owes $" + dollars + " dollars " + dimes + " dimes and " + pennies + " pennies");  //prints out final statement







}  //end of main method   
  	} //end of class
