
public class Arithmetic{
  public static void main (String []args){
  //Number of pairs of pants
  int numPants = 3;
  //Cost per pair of pants
  double pantsPrice = 34.98;
  
  //Number of sweatshirts
  int numShirts = 2;
  //Cost per shirt
  double shirtPrice = 24.99;

  //Number of belts
  int numBelts = 1;
  //cost per belt
  double beltCost = 33.99;
  
  //the tax rate
  double paSalesTax = 0.06;
    
  double SalesTaxOnPants = pantsPrice * paSalesTax;
  //tax on buying pants
  
  double SalesTaxOnShirt = shirtPrice * paSalesTax;
  //tax on buying a shirt
  
  double SalesTaxOnBelt = beltCost * paSalesTax;
  //tax on buying a belt
  
  double totcostpants = numPants * (pantsPrice + SalesTaxOnPants);
  //the total cost of pants w/ sales tax
    
  double totcostshirts = numShirts * (shirtPrice + SalesTaxOnShirt);
  //the total cost of shirts w sales tax
    
  double totcostbelts = numBelts * (beltCost + SalesTaxOnBelt);
  //the total cost of belts w sales tax
    
  double TotCost = (numPants * pantsPrice) + (numShirts * shirtPrice) + (numBelts * beltCost);
  //total cost w/o tax
    
  double TotTax = (numPants * SalesTaxOnPants) + (numShirts * SalesTaxOnShirt) + (numBelts * SalesTaxOnBelt);
  //total tax
    
  double TotPaid = (TotCost + TotTax);
  //total paid with the tax 
  
  System.out.println("The price of 1 pants is: $" + pantsPrice);
  //prints the price of 1 pants
  System.out.println("The price of 1 shirt is: $" + shirtPrice);
  //prints the price of 1 shirt
  System.out.println("The price of 1 belt is: $" + beltCost);
  //prints the price of 1 belt
  System.out.printf("The price of 1 pants with the sales tax is: $%.2f.%n", + (pantsPrice + SalesTaxOnPants));
  //prints the sales tax for 1 pants
  System.out.printf("The price of 1 shirt with the sales tax is: $%.2f.%n", + (shirtPrice + SalesTaxOnShirt));
  //prints the sales tax for 1 shirt
  System.out.printf("The price of 1 belt with the sales tax is: $%.2f.%n", + (beltCost + SalesTaxOnBelt));
  //prints the sales tax for 1 belt
  System.out.printf("The price of " + numPants + " pants with the sales tax is: $%.2f.%n", + totcostpants);
  //prints the price of 3 pants including the tax
  System.out.printf("The price of " + numShirts + " shirt(s) with the sales tax is: $%.2f.%n", + totcostshirts);
  //prints the price of 2 shirts including the tax
  System.out.printf("The price of " + numBelts + " belt(s) with the sales tax is: $%.2f.%n", + totcostbelts);
  //prints the price of 1 belt including the tax
  System.out.printf("The price of " + numPants + " pants, " + numShirts + " shirt(s), and " + numBelts + " belt(s) without sales tax is : $%.2f.%n", + TotCost);
  //prints the price of 3 pants 2 shirts and 1 belt without the tax
  System.out.printf("The total sales tax is: $%.2f.%n", + TotTax);
  //prints the total tax for 3 pants, 2 shirts, and 1 belt
  System.out.printf("The total cost of the purchase is: $%.2f.%n", + TotPaid);
  //prints the total price including the tax
    
  

  }
}