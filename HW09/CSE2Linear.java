import java.util.*; //imports required utilities
public class CSE2Linear {
	public static String ScoreSearch(int[] list, int key) { //used the given binary search method
		int low = 0;
		int count = 1;
		
		int high = list.length-1;
		while(high >= low) {
			int mid = (low + high)/2;
			if (key < list[mid]) {
				high = mid - 1;
				count++;
			}
			else if (key == list[mid]) {
				String found  = key + " was found in the list with " + count + " iterations";
				return found;
				
			}
			else {
				low = mid + 1;
				count++;
			}
		}
		String notfound = key + " was not found in the list with " + count + " iterations";
		String found = key + " was found in the list with " + count + " iterations";
		return notfound;
	}

	public static void main (String[] args) //main metohd
	{
		Scanner sc = new Scanner(System.in);
		final int size = 15;
		int num = 0;
		int[] searchthis = new int[size];
		
		
		for (int i = 0; i < searchthis.length; i++) //loops 15 times for 15 ascending grades
		{
			System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
			if (sc.hasNextInt() == true) //checks for int input, if not, prints out error #1
			{
				num = sc.nextInt();
				
				if (num <= 100 && num > 0) //checks if the int is in range. if not, prints out error #2
				{
					searchthis[i] = num;
					
					if (i == 0)
					{
						searchthis[i] = num;
						continue;
					}
					
					if (num >= searchthis[i - 1]) //checks if the entered value is larger than previous value. if not, prints out error #3
					{
						searchthis[i] = num;
					}
					
					else
					{
						i = i - 1;
						System.out.println("Ascending order, Please. "); //error #3
						sc.nextLine();
					}
				}
				
				else
				{
					i = i - 1;
					System.out.println("out of bounds..."); //error #2
					sc.nextLine();
				}
			}
			else
			{
				System.out.println("Not an integer"); //error #1
			}
		}
		
		for (int i = 0; i < searchthis.length; i++)
		{
			System.out.print(searchthis[i] + " "); //searching from the array
		}
		System.out.println("");
		System.out.println("Enter a grade to search for: ");
		System.out.println(ScoreSearch(searchthis, sc.nextInt()));
	}
}