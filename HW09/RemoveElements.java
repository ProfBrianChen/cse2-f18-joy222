import java.util.*;
import java.lang.*;
import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils; //imports needed utilities *****codeanywhere doesn't allow to use the import Arrayutils; I consulted the teacher
public class RemoveElements{
	public static int[] randomInput() //random array range of 0-9
	{
		int[] newarray = new int[10];
		for (int i = 0; i < newarray.length; i++)
		{
			newarray[i] = ((int)(Math.random()*9 + 1));
		}
		return newarray;
	}
	
	public static int[] delete(int[] list, int pos) //deletes desired element in position
	{
		int[] deleted = new int[9];
		if (pos > -1 && pos < 10)
		{
			deleted = ArrayUtils.removeElement(list, list[pos]); //if in range, remove element
		}
		else
		{
			System.out.println("The index is not valid."); //if not in the range, prints out error
			deleted = list;
		}
		
		return deleted;
	}
	
	public static int[] remove(int[] list, int target) //remove desired elements from the array
	{
		int listlength = list.length;
		for (int i = list.length-1; i >= 0; i--)
		{
			if (list[i] == target)
			{
				list = ArrayUtils.remove(list, i); //loops from the back so it counts multiple occurrences
			}
		}
		
		if (listlength != list.length)
		{
			System.out.println("Element " + target + " has been found"); //if elements are removed, print this
		}
		else
		{
			System.out.println("Element " + target + " was not found"); //if not found, print this
		}
		return list;
	}
	public static void main(String [] arg) //main methods (given)
	{
		Scanner scan=new Scanner(System.in);
		int num[]=new int[10];
		int newArray1[];
		int newArray2[];
		int index,target;
		String answer="";
		do{
		System.out.print("Random input 10 ints [0-9]");
		num = randomInput();
		String out = "The original array is:";
		  	out += listArray(num);
		  	System.out.println(out);
		 
		  	System.out.print("Enter the index ");
		index = scan.nextInt();
		newArray1 = delete(num,index);
		String out1="The output array is ";
		out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
		  	System.out.println(out1);
		 System.out.println("Index " + index + " element is removed.");
		      System.out.print("Enter the target value ");
		target = scan.nextInt();
		newArray2 = remove(num,target);
		String out2="The output array is ";
		out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
		System.out.println(out2);
		System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
		answer=scan.next();
		}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
 
