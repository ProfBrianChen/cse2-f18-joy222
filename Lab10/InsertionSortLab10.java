import java.util.Arrays;
public class InsertionSortLab10 {
	public static void main (String[] args) {
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest = insertionSort(myArrayBest);
		int iterWorst = insertionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}
	
	public static int insertionSort(int[] list)
	{
		//prints the initial array ( I have to insert another print statement later)
		System.out.println(Arrays.toString(list));
		//initializing the counter
		int iterations = 0;
		//element list[i] for the array
		for (int i = 1; i < list.length; i++)
		{
			iterations++;
			//sorts the array
			for (int j = i; j > 0; j--)//swaps element
			{
				if(list[j] < list[j-1])
				{
					int temp = list[j];
					list[j] = list[j-1];
					list[j-1] = temp;
					iterations++;//iteration adds
				}
				else {
					break;
				}
			}
			System.out.println(Arrays.toString(list));
		}
		return iterations;
	}
}
