import java.util.Arrays;
public class SelectionSortLab10 {
	public static void main (String[] args)
	{
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest = selectionSort(myArrayBest);
		int iterWorst = selectionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}


 //method for sorting the numbers/
	public static int selectionSort(int[] list)
	{
		//print out another print out statement to show that it's being sorted
		System.out.println(Arrays.toString(list));
		//initializing the counter
		int iterations = 0;
		
		for (int i = 0; i < list.length-1; i++)
		{
			iterations++;
			//finding the minimum
			int currentMin = list[i];
			int currentMinIndex = i;
			for (int j = i + 1; j < list.length; j++)
			{
				if (list[j] < list[currentMinIndex])
				{
					currentMin = list[j];
					currentMinIndex = j;
				}
				iterations++;
			}
			if (currentMinIndex != i) //if the min index is not in the right place, swap
			{
				int temp = list[i];
				list[i] = list[currentMinIndex];
				list[currentMinIndex] = temp;
				System.out.println(Arrays.toString(list));
			}
		}
		return iterations;
	}
}
	
