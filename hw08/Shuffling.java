import java.util.Arrays; //imports arrays and scanner
import java.util.Scanner;
public class Shuffling{ 
	public static void printArray(String[] list) //printarray method
	{
		System.out.print(Arrays.toString(list) + " "); //prints out the array
		System.out.println("");
	}
	public static void shuffle(String[] list) //shuffle method
	{
		for (int i = 0; i < 50; i++) //randomly shuffles the array with the cards
		{
			String temp = "";
			int randindex = (int)(Math.random()*51+1);
			temp = list[0];
			list[0] = list[(randindex)];
			list[randindex] = temp;			
		}
	}
	
	public static String[] getHand(String[] list, int cardindex, int numWanted) //gets hand of 5 cards starting from the index of 51
	{
		String[] hand = new String[numWanted];
		for (int i = 0; (i + cardindex - numWanted) < cardindex; i++)
		{
			hand[i] = list[cardindex-i];
			
		}
		
		return hand;
	}
	
	public static String[] getCards() //giving a set of 52 cards to an array
	{
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		
		for (int i=0; i<52; i++)
		{ 
			cards[i]=rankNames[i%13]+suitNames[i/13];
		}
		
		return cards;
	}
	
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
	
	String[] hand = new String[5]; 
	int numCards = 5; //assigning variables
	int again = 1; 
	int index = 51;
	String[] cards = getCards();
	System.out.println();
	printArray(cards);
	System.out.println("Shuffled");
	shuffle(cards); 
	printArray(cards);
	System.out.println("Enter the number of cards for your hand: "); //asks the user for the cards in your hand
	numCards = scan.nextInt();
	while(again == 1){ //while loop to ask the user if she/he wants another hand
		System.out.println("Hand");
		printArray(getHand(cards,index,numCards));
		index = index - numCards;
		System.out.println("Enter a 1 if you want another hand drawn"); 
		if (numCards > index) //if the number of cards are less than 5, create a new set and pick from there
		{
			cards = getCards();
			shuffle(cards);
			index = 51;
			System.out.println("Cards were reset");
		}
		again = scan.nextInt();
		
		}  
  } 
}
